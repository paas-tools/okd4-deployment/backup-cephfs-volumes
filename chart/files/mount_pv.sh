#!/usr/bin/env bash

validateVar(){
  if [ -z "${1}" ]; then echo "Failed to retrieve value ${2}" 1>&2; exit 1; fi
}

# Will stop the execution of the backup script if it finds any command execution error
# as all the operations are critical.
set -e

PV_NAME="$1"
validateVar "$PV_NAME" "PV_NAME (ARG 1)"
MOUNT_POINT="$2"
validateVar "$MOUNT_POINT" "MOUNT_POINT (ARG 2)"
EXTRA_MOUNT_OPTIONS="${3:-}" # optional

# Contact the OpenStack manila API to retrieve information about each of the manila shares
# We need this to be able to mount PVs for backup
# See https://clouddocs.web.cern.ch/file_shares/programmatic_access.html
REGION_NAME=$(openstack configuration show -f json | jq -e -r .region_name)
MANILA_URL=$(openstack catalog show manilav2 -f json | jq -e -r --arg REGION_NAME "$REGION_NAME" '.endpoints[] | select(.interface == "public" and .region == $REGION_NAME) | .url')
validateVar "$MANILA_URL" "MANILA_URL"

# OpenStack token issues will expire after 24h, so we can create several tokens per day
OPENSTACK_MANILA_SECRET=$(openstack token issue -f json | jq -e -r '.id')
validateVar "$OPENSTACK_MANILA_SECRET" "OPENSTACK_MANILA_SECRET"

PV_JSON=$(oc get pv/$PV_NAME -o json)
validateVar "$PV_JSON" "PV_JSON"

NAMESPACE_CSI_DRIVER=$(echo "$PV_JSON" | jq -e -r '.spec.csi.nodeStageSecretRef.namespace')
validateVar "$NAMESPACE_CSI_DRIVER" "NAMESPACE_CSI_DRIVER"

# We need this information to access the manila API
MANILA_SHARE_ID=$(echo "$PV_JSON" | jq -e -r '.spec.csi.volumeAttributes.shareID')
validateVar "$MANILA_SHARE_ID" "MANILA_SHARE_ID"

MANILA_SHARE_ACCESS_ID=$(echo "$PV_JSON" | jq -e -r '.spec.csi.volumeAttributes.shareAccessID')
validateVar "$MANILA_SHARE_ACCESS_ID" "MANILA_SHARE_ACCESS_ID"

MANILA_EXPORT_LOCATIONS=$(curl -sS --fail -X GET -H "X-Auth-Token: $OPENSTACK_MANILA_SECRET" -H "X-Openstack-Manila-Api-Version: 2.51" "${MANILA_URL}/shares/${MANILA_SHARE_ID}/export_locations")
validateVar "$MANILA_EXPORT_LOCATIONS" "MANILA_EXPORT_LOCATIONS"

# Stores monitors and path of the PV, similar to
# 137.138.121.135:6789,188.184.85.133:6789,188.184.91.157:6789:/volumes/_nogroup/337f5361-bee2-415b-af8e-53eaec1add43
# we expect a single export location; even if there are multiple, just pick the first one
# https://github.com/kubernetes/cloud-provider-openstack/blob/master/pkg/csi/manila/util/exportlocation.go#L39
CEPHFS_PATH_PV=$(echo "${MANILA_EXPORT_LOCATIONS}" | jq -e -r '.export_locations[0].path')
validateVar "$CEPHFS_PATH_PV" "CEPHFS_PATH_PV"

# Stores the userKey credentials needed to manually mount CephFS PVs
MANILA_ACCESS_RULES=$(curl -sS --fail -X GET -H "X-Auth-Token: $OPENSTACK_MANILA_SECRET" -H "X-Openstack-Manila-Api-Version: 2.51" "${MANILA_URL}/share-access-rules/${MANILA_SHARE_ACCESS_ID}")
validateVar "$MANILA_ACCESS_RULES" "MANILA_ACCESS_RULES"

CEPHFS_USERKEY=$(echo "${MANILA_ACCESS_RULES}" | jq -e -r '.access.access_key')
validateVar "$CEPHFS_USERKEY" "CEPHFS_USERKEY"

MOUNT_OPTIONS="name=${PV_NAME},secret=${CEPHFS_USERKEY}"
if [ -n "${EXTRA_MOUNT_OPTIONS}" ]; then
    MOUNT_OPTIONS="${MOUNT_OPTIONS},${EXTRA_MOUNT_OPTIONS}"
fi

mount -t ceph "$CEPHFS_PATH_PV" -o "$MOUNT_OPTIONS" "$MOUNT_POINT"

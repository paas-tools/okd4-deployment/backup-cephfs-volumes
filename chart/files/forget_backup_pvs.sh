#!/usr/bin/env bash

timestamp() {
  date +%Y-%m-%dT%H:%M:%S.%3NZ
}

# Will stop the execution of the forget backup script if it finds any command execution error
set -e

# Get job name uid through the downward API. This value is store in the labels of the pod just created by the job.
# It is required to run parallel pods in the job and be able to do simultaneously forget backups in parallel of different PVs.
JOB_UID=$(grep -F 'batch.kubernetes.io/job-name=' /etc/jobinfo/labels | cut -d'=' -f2 |  tr -d '"')

# Iterates over all the items of the repo queue identified by the job id and the init name.
while true; do
  PV_JSON=$(redis-cli -h redis LPOP job-${JOB_UID}-${REDIS_QUEUE_INIT_NAME}-queue)
  if [ -z "$PV_JSON" ]; then
    echo "No more restic folders to process"
    exit 0
  fi

  # Get information needed for each of the json queue elements of the repo.
  # This is needed to mount the PVs into the pods to do the forget backup.
  PV_NAME=$(echo "$PV_JSON" | jq -r '.metadata.name')
  echo "Processing PV ${PV_NAME} ..."

  # We need to export RESTIC_REPOSITORY to a new path as we now backup each of the PVs
  # separately into a different folder per PV (See https://its.cern.ch/jira/browse/CIPAAS-605)
  export RESTIC_REPOSITORY="${RESTIC_REPO_BASE}/${PV_NAME}"

  # Check if restic repo exists, if there is no restic repo, there is nothing to forget
  if ! restic list locks; then
    echo "INFO there is no restic repo for PV ${PV_NAME}, probably PV was never backed up and there is nothing to forget.."
    continue
  fi

  # remove any stale lock (e.g. failed backups)
  restic unlock

  # Run restic check to verify that all data is properly stored in the repo.
  if ! restic check; then
    echo "ERROR when checking restic data, it seems the PV ${PV_NAME} data is not properly stored in the repository"
    oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-check-failure-at="$(timestamp)" --overwrite=true
    oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-check-failure-by="$(hostname)" --overwrite=true
    continue
  else
    # Push metrics into the prometheus group identified by cephfs_volume_last_backup_check{job="cephfs_forget_backup_pv", persistentvolume="pv_name", status="backup_check_succeeded"} date +%s
    curl -sS --data-binary @- "${PUSHGATEWAY_SERVICE}/metrics/job/cephfs_forget_backup_pv/persistentvolume/${PV_NAME}/status/backup_check_succeeded" <<EOF
        # TYPE cephfs_volume_last_backup_check gauge
        # HELP cephfs_volume_last_backup_check job="cephfs_forget_backup_pv" persistentvolume="pv_name" status="backup_check_succeeded"
        cephfs_volume_last_backup_check $(date '+%s.%N' | sed 's/N$//')
EOF

    PV_STATUS_PHASE=$(echo "$PV_JSON" | jq -r '.status.phase')
    if [ "$PV_STATUS_PHASE" == "Released" ]; then

      # forget and prune all backups but one
      # Both forget and prune need the exclusive lock on the whole restic repo in S3 (cannot run concurrently with backups)
      # so we do both operations together
      # We apply different policy for PVs already Released, so we delete all backups but one
      echo "Forgetting backups for a released PV..."
      restic forget ${restic_forget_args_pv_released}
    else
      # forget and prune old backups
      # Both forget and prune need the exclusive lock on the whole restic repo in S3 (cannot run concurrently with backups)
      # so we do both operations together
      echo "Forgetting backups..."
      restic forget ${restic_forget_args}
    fi

    # It annotates the success of the forget backup into the PV
    echo annotating and labeling PV "$PV_NAME" JOB_UID: "$JOB_UID" ...
    oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-forget-success-at="$(timestamp)" --overwrite=true
    oc annotate pv/"$PV_NAME" backup-cephfs-volumes.cern.ch/backup-forget-success-by="$(hostname)" --overwrite=true

    # Push metrics into the prometheus group identified by cephfs_volume_last_backup_forget{job="cephfs_forget_backup_pv", persistentvolume="pv_name", status="backup_forget_succeeded"} date +%s
    curl -sS --data-binary @- "${PUSHGATEWAY_SERVICE}/metrics/job/cephfs_forget_backup_pv/persistentvolume/${PV_NAME}/status/backup_forget_succeeded" <<EOF
        # TYPE cephfs_volume_last_backup_forget gauge
        # HELP cephfs_volume_last_backup_forget job="cephfs_forget_backup_pv" persistentvolume="pv_name" status="backup_forget_succeeded"
        cephfs_volume_last_backup_forget $(date '+%s.%N' | sed 's/N$//')
EOF
  fi

  # We remove /root/.cache/ in each iteration to prevent restic forget backups to run out of memory and fail the cronjobs we run,
  # as we detected this malfunction in our infra.
  echo "cleaning up /root/.cache/*"
  rm -rf /root/.cache/*

done

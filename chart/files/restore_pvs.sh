#!/usr/bin/env bash

PV_NAMES="$@"

timestamp() {
  date +%Y-%m-%dT%H:%M:%S.%3NZ
}

validateVar(){
  if [ -z "${1}" ]; then echo "Failed to retrieve value ${2}" 1>&2; exit 1; fi
}

# Will stop the execution of the backup script if it finds any command execution error
# as all the operations are critical.
set -e

# Contact the OpenStack manila API to retrieve information about each of the manila shares
# We need this to be able to mount PVs for backup
# See https://clouddocs.web.cern.ch/file_shares/programmatic_access.html
REGION_NAME=$(openstack configuration show -f json | jq -e -r .region_name)
MANILA_URL=$(openstack catalog show manilav2 -f json | jq -e -r --arg REGION_NAME "$REGION_NAME" '.endpoints[] | select(.interface == "public" and .region == $REGION_NAME) | .url')
validateVar "$MANILA_URL" "MANILA_URL"

# Tokens issued by OpenStack will expire after 24h, so we can create several tokens per day
OPENSTACK_MANILA_SECRET=$(openstack token issue -f json | jq -e -r '.id')
validateVar "$OPENSTACK_MANILA_SECRET" "OPENSTACK_MANILA_SECRET"

# Iterates over PVs pairs (SOURCE:TARGET) specified on the command line
for PAIR in $PV_NAMES; do
      SOURCE_PV=$(echo "$PAIR" | cut -d: -f1)
      validateVar "$SOURCE_PV" "SOURCE_PV"
      TARGET_PV=$(echo "$PAIR" | cut -d: -f2)
      validateVar "$TARGET_PV" "TARGET_PV"

      export RESTIC_REPOSITORY="${RESTIC_REPO_BASE}/${SOURCE_PV}"
      # Check if there are any backups for the source volume (maybe not be the case if it is new etc.)
      if ! restic snapshots > /dev/null; then
        echo "No snapshots found for restic repository ${RESTIC_REPOSITORY}, skipping"
        continue
      fi

      echo "mounting '${TARGET_PV}' at '${MOUNT_POINT}' on node '${NODE_NAME:-unknown}' ..."
      /scripts/mount_pv.sh "$TARGET_PV" "/mnt"

      # Generally we only ever restore into an empty directory,
      # but in some cases it may be necessary (e.g. partially restored volume).
      if  [ "$FORCE_OVERWRITE" != "true" ]; then
          if ls -A1q /mnt | grep -q .; then
              echo "PV $TARGET_PV is not empty, refusing to restore."
              umount /mnt
              exit 1
          fi
      fi

      echo "restoring latest snapshot of PV $SOURCE_PV ..."
      if ! restic restore latest --target=/; then
        echo "ERROR restoring pv $TARGET_PV from $SOURCE_PV"
        umount /mnt
        exit 1
      fi

      echo "PV $TARGET_PV successfully restored $(df -h --output=used /mnt | grep -v Used) from $SOURCE_PV"
      oc annotate pv/"$TARGET_PV" backup-cephfs-volumes.cern.ch/restore-success-at="$(timestamp)" --overwrite=true

      # Unmount pv from /mnt earlier mounted
      echo "unmounting $TARGET_PV from /mnt ..."
      umount /mnt

      # We remove /root/.cache/ in each iteration to prevent restic backups to run out of memory and fail the cronjobs we run,
      # as we detected this malfunction in our infra.
      echo "cleaning up /root/.cache/*"
      rm -rf /root/.cache/*
done
